package cat.itb.testingitb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    TextView title;
    EditText username, password;
    Button button;

    public static String USER_TO_BE_TYPED = "sergio";
    public static String PASS_TO_BE_TYPED =  "password123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.actionButtonMainActivity);
        title = findViewById(R.id.mainActivityTitle);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // button.setText("Logged");
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
            }
        });


    }
}