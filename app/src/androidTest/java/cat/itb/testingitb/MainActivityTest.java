package cat.itb.testingitb;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static cat.itb.testingitb.MainActivity.PASS_TO_BE_TYPED;
import static cat.itb.testingitb.MainActivity.USER_TO_BE_TYPED;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

   /* @Test
    public void display_correctly() {

        onView(withId(R.id.mainActivityTitle)).check(matches(isDisplayed()));

        onView(withId(R.id.actionButtonMainActivity)).check(matches(isDisplayed()));

    }*/

   /*@Test no funciona porque se ha cambiado el texto para los siguientes ejercicios
    public void checkItemsTextIsCorrect() {

        onView(withId(R.id.mainActivityTitle)).check(matches(withText("Next")));


    }

    @Test no funciona porque se ha cambiado el texto para los siguientes ejercicios
    public void checkItemsTextIsCorrectBarista(){
        assertDisplayed(R.id.mainActivityTitle, "Next");
    }*/


    /*@Test no funciona porque se ha cambiado el texto para los siguientes ejercicios
    public void checkClickableButtonAndButtonTextChanges() {

        onView(withId(R.id.actionButtonMainActivity)).check(matches(isClickable()));

        onView(withId(R.id.actionButtonMainActivity)).perform(click());

        onView(withId(R.id.actionButtonMainActivity)).check(matches(withText("Back")));


    }*/

    /*@Test no funciona porque se ha cambiado el texto para los siguientes ejercicios
    public void login_form_behaviour() {

        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard());

        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.actionButtonMainActivity)).perform(click()).check(matches(withText("Logged")));

    }*/


   /* @Test
    public void toSecondActivity() {


        onView(withId(R.id.actionButtonMainActivity)).perform(click());
        onView(withId(R.id.secondActivity)).check(matches(isDisplayed()));


    }*/

   /* @Test
    public void toMainActivity() {

        onView(withId(R.id.actionButtonMainActivity)).perform(click());
        onView(withId(R.id.secondActivity)).check(matches(isDisplayed()));

        onView(withId(R.id.actionButtonSecondActivity)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));


        onView(withId(R.id.actionButtonMainActivity)).perform(click());
        Espresso.pressBack();
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));


    }*/

    /*@Test
    public void ex5_large_function() {

        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard()).check(matches(withText(USER_TO_BE_TYPED)));

        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard()).check(matches(withText(PASS_TO_BE_TYPED)));

        onView(withId(R.id.actionButtonMainActivity)).perform(click());

        onView(withId(R.id.secondActivity)).check(matches(isDisplayed()));

        onView(withId(R.id.mainActivityTitle)).check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));

        onView(withId(R.id.actionButtonSecondActivity)).perform(click());

        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));

        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));


    }*/


}
